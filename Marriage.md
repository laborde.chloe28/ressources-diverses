# Marriage and friend relationship

In dotted (- - -) edge, you have friend/adoption relationship, and in normal (---) edge, you have marriage relationship between people of the inter-insa discord server. In strong edge, you have IRL relationship.

```mermaid
graph TD;
  classDef CVL fill:#9444ff;
  classDef Espion fill:#979c9f;
  classDef Lyon fill:#f1c40f;
  classDef Rennes fill:#ff7d00;
  classDef Rouen fill:#00a5ff;
  classDef Toulouse fill:#ff0000;
  classDef HDF fill:#fc9eff;
  classDef Strasbourg fill:#2ecc71
  classDef EuroMed fill:#e91e63;

  A([Zo]):::Espion-.-B([Sato]):::Espion;
  A---C([Mamine]):::Rouen;
  A---D([Laura]):::Toulouse;
  A---E([Tyney]):::Toulouse;
  C-.-F([Trino]):::CVL;
  C---E;
  D---E;
  B---E;
  E===G([Kivero]):::Espion;
  E-.-H([Scipi0]):::Rennes;
  I([Mople]):::Toulouse===J([Catnip]):::Rennes;
  J-.-H;
  K([Jeanne]):::Rennes===L([Keyro]):::Espion;
  M([Meta]):::Toulouse---B;
  N([Ae-Chan]):::Espion===O([Aizen]):::Toulouse;
```
